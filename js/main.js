class GameItem {
    constructor(x, y, name) {
        this._xPos = x;
        this._yPos = y;
        this._name = name;
    }
    get element() {
        return this._element;
    }
    get xPos() {
        return this._xPos;
    }
    get yPos() {
        return this._yPos;
    }
    draw(location) {
        this._element = document.createElement("div");
        let image = document.createElement("img");
        image.setAttribute("src", "../assets/images/" + this._name + ".png");
        this._element.className = "character";
        image.style.width = "100%";
        this._element.style.position = "absolute";
        this._element.style.top = this._yPos + "px";
        this._element.style.left = this._xPos + "px";
        this._element.appendChild(image);
        location.appendChild(this._element);
    }
    move(amountx, amounty) {
        this._xPos += amountx;
        this._yPos += amounty;
        this._element.style.left = this._xPos + "px";
        this._element.style.top = this._yPos + "px";
    }
}
class Bullet extends GameItem {
    constructor(up) {
        super(-20, -20, "bullet");
        this._width = 10;
        this._height = 20;
        this._element = document.createElement("div");
        this._element.style.width = this._width + "px";
        this._element.style.height = this._height + "px";
        this._element.style.display = "none";
        this._element.style.position = "absolute";
        this._element.style.backgroundImage = "url('../assets/images/laser.png')";
        this._up = up;
    }
    setPosition(x, y) {
        this._xPos = x;
        this._yPos = y;
    }
    draw() {
        if (this._up) {
            this.move(0, -10);
        }
        else {
            this.move(0, 10);
        }
    }
    get width() {
        return this._width;
    }
}
class Character extends GameItem {
    constructor(name, x, y, bulletPool) {
        super(x, y, name);
        this.right = () => {
            this.move(8, 0);
            if (this._xPos > document.body.clientWidth - this._element.clientWidth) {
                this._xPos = document.body.clientWidth - this._element.clientWidth;
            }
        };
        this.left = () => {
            this.move(-8, 0);
            if (this._xPos < 0) {
                this._xPos = 0;
            }
        };
        this.up = () => {
            this.move(0, -8);
            if (this._yPos < 0) {
                this._yPos = 0;
            }
            if (!this._up) {
                this._element.style.transform = "scaleY(-1)";
                this._up = true;
            }
        };
        this.down = () => {
            this.move(0, 8);
            if (this._yPos > document.body.clientHeight - this._element.clientHeight) {
                this._yPos = document.body.clientHeight - this._element.clientHeight;
            }
            if (this._up) {
                this._element.style.transform = "scaleY(1)";
                this._up = false;
            }
        };
        this.shoot = () => {
            if (!this._dead) {
                if (this._bulletTime === 0) {
                    let bullet;
                    if (this._up) {
                        bullet = new Bullet(true);
                        bullet.setPosition(this._xPos + this._element.clientWidth / 2 - bullet.width / 2, this._yPos - 10);
                    }
                    else {
                        bullet = new Bullet(false);
                        bullet.setPosition(this._xPos + this._element.clientWidth / 2 - bullet.width / 2, this._yPos + this._element.clientHeight + 5);
                    }
                    bullet.element.style.display = "block";
                    this._bulletPool.push(bullet);
                    this._bulletTime = 40;
                    console.log(this._name + " shoots");
                }
            }
        };
        this._bulletPool = bulletPool;
        this._bulletTime = 0;
        this._invincibleTime = 0;
        this._dead = false;
        this._up = false;
    }
    invincible() {
        if (this._element.style.display === "none") {
            this._element.style.display = "block";
        }
        else {
            this._element.style.display = "none";
        }
    }
    get element() {
        return this._element;
    }
    get bulletTime() {
        return this._bulletTime;
    }
    set bulletTime(amount) {
        this._bulletTime = amount;
    }
    get invincibleTime() {
        return this._invincibleTime;
    }
    set invincibleTime(amount) {
        this._invincibleTime = amount;
    }
    set bulletPool(bulletPool) {
        this._bulletPool = bulletPool;
    }
    set dead(status) {
        this._dead = status;
    }
}
class Game {
    constructor() {
        this._element = document.getElementById('container');
        this.bulletPool = [];
        this.obstaclePool = [];
        this.update = () => {
            if (!this._pause) {
                requestAnimationFrame(this.update);
                this.background();
                this.keyHandler.inputHandler();
                this.bulletHandler();
                this.obstacleCreator();
                this.obstacleHandler();
                this.collisionHandler();
            }
        };
        this._wallpap = 0;
        this._winner = "";
        this._pause = false;
        this._hud = new HUD();
        this.init();
        this.update();
    }
    init() {
        this._element.style.width = document.body.clientWidth + "px";
        this._element.style.height = document.body.clientHeight + "px";
        this.keyHandler = new KeyHandler();
        this._character1 = new Character('character1', 100, 400, this.bulletPool);
        this._character2 = new Character('character2', 1000, 400, this.bulletPool);
        this._character1.draw(this._element);
        this._character2.draw(this._element);
        this.keyHandler.addCallback(39, this._character1.right);
        this.keyHandler.addCallback(37, this._character1.left);
        this.keyHandler.addCallback(38, this._character1.up);
        this.keyHandler.addCallback(40, this._character1.down);
        this.keyHandler.addCallback(77, this._character1.shoot);
        this.keyHandler.addCallback(65, this._character2.left);
        this.keyHandler.addCallback(68, this._character2.right);
        this.keyHandler.addCallback(87, this._character2.up);
        this.keyHandler.addCallback(83, this._character2.down);
        this.keyHandler.addCallback(67, this._character2.shoot);
        this._hud.init(this._element);
        this._audio = new Audio('assets/sound/150413_Magical_Night---free_download.mp3');
        this._audio.play();
    }
    bulletHandler() {
        for (let i = 0; i < this.bulletPool.length; i++) {
            if (this.bulletPool[i] === null) {
                continue;
            }
            if (this.bulletPool[i].element.parentElement === null) {
                this._element.appendChild(this.bulletPool[i].element);
            }
            else {
                this.bulletPool[i].draw();
                if (this.bulletPool[i].yPos < 0 || this.bulletPool[i].yPos > document.body.clientHeight) {
                    this._element.removeChild(this.bulletPool[i].element);
                    this.bulletPool[i] = null;
                }
            }
        }
        this.bulletPool = this.bulletPool.filter(x => x !== null);
        this._character1.bulletPool = this.bulletPool;
        this._character2.bulletPool = this.bulletPool;
        if (this._character1.bulletTime != 0) {
            this._character1.bulletTime = this._character1.bulletTime - 1;
        }
        if (this._character2.bulletTime != 0) {
            this._character2.bulletTime = this._character2.bulletTime - 1;
        }
        if (this._character1.invincibleTime != 0) {
            this._character1.invincibleTime = this._character1.invincibleTime - 1;
            this._character1.invincible();
        }
        if (this._character2.invincibleTime != 0) {
            this._character2.invincibleTime = this._character2.invincibleTime - 1;
            this._character2.invincible();
        }
    }
    collisionHandler() {
        for (let i = 0; i < this.bulletPool.length; i++) {
            if (((this.bulletPool[i].yPos < this._character1.yPos + this._character1.element.clientHeight) && (this.bulletPool[i].yPos > this._character1.yPos)) && ((this.bulletPool[i].xPos < this._character1.xPos + this._character1.element.clientWidth) && (this.bulletPool[i].xPos > this._character1.xPos)) && this._character1.invincibleTime === 0) {
                console.log("player1 hit by bullet");
                this._hud.removeHeart(true);
                this._character1.invincibleTime = 100;
                if (this._hud.getLives(true) === 0) {
                    this._element.removeChild(this._character1.element);
                    this._character1.dead = true;
                    this._winner = "Player 2";
                    this.win();
                }
            }
            if (((this.bulletPool[i].yPos < this._character2.yPos + this._character2.element.clientHeight) && (this.bulletPool[i].yPos > this._character2.yPos)) && ((this.bulletPool[i].xPos < this._character2.xPos + this._character2.element.clientWidth) && (this.bulletPool[i].xPos > this._character2.xPos)) && this._character2.invincibleTime === 0) {
                console.log("player2 hit by bullet");
                this._hud.removeHeart(false);
                this._character2.invincibleTime = 100;
                if (this._hud.getLives(false) === 0) {
                    this._element.removeChild(this._character1.element);
                    this._character1.dead = true;
                    this._winner = "Player 1";
                    this.win();
                }
            }
        }
        for (let i = 0; i < this.obstaclePool.length; i++) {
            if ((((this._character1.yPos >= this.obstaclePool[i].yPos && this._character1.yPos <= this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight) || (this._character1.yPos + this._character1.element.clientHeight >= this.obstaclePool[i].yPos && this._character1.yPos + this._character1.element.clientHeight <= this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight)) || ((this.obstaclePool[i].yPos >= this._character1.yPos && this.obstaclePool[i].yPos <= this._character1.yPos + this._character1.element.clientHeight) || (this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight >= this._character1.yPos && this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight <= this._character1.yPos + this._character1.element.clientHeight))) && (this.obstaclePool[i].xPos >= this._character1.xPos && this.obstaclePool[i].xPos <= this._character1.xPos + this._character1.element.clientWidth) && this._character1.invincibleTime === 0) {
                console.log("player1 hit by obstacle");
                this._hud.removeHeart(true);
                this._character1.invincibleTime = 100;
                if (this._hud.getLives(true) === 0) {
                    this._element.removeChild(this._character1.element);
                    this._character1.dead = true;
                    this._winner = "Player 2";
                    this.win();
                }
            }
            if ((((this._character2.yPos >= this.obstaclePool[i].yPos && this._character2.yPos <= this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight) || (this._character2.yPos + this._character2.element.clientHeight >= this.obstaclePool[i].yPos && this._character2.yPos + this._character2.element.clientHeight <= this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight)) || ((this.obstaclePool[i].yPos >= this._character2.yPos && this.obstaclePool[i].yPos <= this._character2.yPos + this._character2.element.clientHeight) || (this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight >= this._character2.yPos && this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight <= this._character2.yPos + this._character2.element.clientHeight))) && (this.obstaclePool[i].xPos >= this._character2.xPos && this.obstaclePool[i].xPos <= this._character2.xPos + this._character2.element.clientWidth) && this._character2.invincibleTime === 0) {
                console.log("player2 hit by obstacle");
                this._hud.removeHeart(false);
                this._character2.invincibleTime = 100;
                if (this._hud.getLives(false) === 0) {
                    this._element.removeChild(this._character1.element);
                    this._character1.dead = true;
                    this._winner = "Player 1";
                    this.win();
                }
            }
        }
    }
    obstacleCreator() {
        let random = Math.random() * 600;
        if (random > 595) {
            let sides = Math.random() * 200;
            while (sides < 20) {
                sides = Math.random() * 200;
            }
            let obstacle = new Obstacle(sides, sides);
            let pos = Math.random() * document.body.clientHeight;
            obstacle.setPosition(document.body.clientWidth, pos);
            this._element.appendChild(obstacle.element);
            this.obstaclePool.push(obstacle);
        }
    }
    obstacleHandler() {
        for (let i = 0; i < this.obstaclePool.length; i++) {
            this.obstaclePool[i].draw();
            if (this.obstaclePool[i].xPos < 0) {
                this._element.removeChild(this.obstaclePool[i].element);
                this.obstaclePool[i] = null;
            }
        }
        this.obstaclePool = this.obstaclePool.filter(x => x !== null);
    }
    background() {
        this._wallpap -= 1;
        let background = document.getElementById("container");
        background.style.backgroundPosition = this._wallpap + 'px 0';
    }
    win() {
        this._pause = true;
        this._element.innerHTML = "";
        let wintext = document.createElement("div");
        let text = document.createElement("p");
        text.innerHTML = "The winner is: " + this._winner + "<br>Herladen om nog een keer te spelen.";
        wintext.appendChild(text);
        wintext.id = "winsign";
        this._element.appendChild(wintext);
        this._audio.pause();
    }
}
class HUD {
    init(location) {
        this._player1life = document.createElement("div");
        this._player1life.classList.add("life");
        this._player2life = document.createElement("div");
        this._player2life.classList.add("life");
        let p1 = document.createElement("p");
        let p2 = document.createElement("p");
        p1.innerText = "Levens";
        p2.innerText = "Levens";
        this._player1life.appendChild(p1);
        this._player2life.appendChild(p2);
        this._player1life.style.left = 0 + "px";
        this._player1life.style.top = 0 + "px";
        let lifebox = [];
        for (let i = 0; i < 3; i++) {
            let box = document.createElement("div");
            box.classList.add("lifebox");
            lifebox.push(box);
        }
        let lifebox2 = [];
        for (let i = 0; i < 3; i++) {
            let box = document.createElement("div");
            box.classList.add("lifebox");
            lifebox2.push(box);
        }
        for (let i = 0; i < lifebox.length; i++) {
            this._player1life.appendChild(lifebox[i]);
        }
        for (let i = 0; i < lifebox2.length; i++) {
            this._player2life.appendChild(lifebox2[i]);
        }
        location.appendChild(this._player2life);
        this._player2life.style.left = document.body.clientWidth - this._player2life.clientWidth + "px";
        location.appendChild(this._player1life);
    }
    removeHeart(playerone) {
        if (playerone) {
            let lifeboxes = this._player1life.getElementsByClassName("lifebox");
            this._player1life.removeChild(lifeboxes[0]);
        }
        else {
            let lifeboxes = this._player2life.getElementsByClassName("lifebox");
            this._player2life.removeChild(lifeboxes[0]);
        }
    }
    getLives(playerone) {
        if (playerone) {
            return this._player1life.getElementsByClassName("lifebox").length;
        }
        else {
            return this._player2life.getElementsByClassName("lifebox").length;
        }
    }
}
class KeyHandler {
    constructor() {
        this._callBacks = {};
        this._keys = {};
        this.keyDownHandler = (event) => {
            if (this._callBacks[event.keyCode] != null) {
                this._keys[event.keyCode] = true;
            }
        };
        this.keyUpHandler = (event) => {
            if (this._callBacks[event.keyCode] != null) {
                this._keys[event.keyCode] = false;
            }
        };
        window.addEventListener("keydown", this.keyDownHandler);
        window.addEventListener("keyup", this.keyUpHandler);
    }
    addCallback(keycode, func) {
        this._callBacks[keycode] = func;
    }
    inputHandler() {
        for (let key in this._keys) {
            if (this._keys[key] === true) {
                this._callBacks[key]();
            }
        }
    }
}
let app;
(function () {
    let init = function () {
        document.getElementById("container").innerHTML = "";
        app = new Game();
    };
    document.getElementById("startbutton").addEventListener('click', init);
})();
class Obstacle extends GameItem {
    constructor(width, height) {
        super(0, 0, "obstacle");
        this._height = height;
        this._width = width;
        this._element = document.createElement("div");
        this._element.style.width = this._width + "px";
        this._element.style.height = this._height + "px";
        let image = document.createElement("img");
        image.src = "./assets/images/asteroid.png";
        image.style.height = "100%";
        this._element.appendChild(image);
        this._element.style.position = "absolute";
    }
    setPosition(x, y) {
        this._xPos = x;
        this._yPos = y;
    }
    draw() {
        this.move(-10, 0);
    }
}
//# sourceMappingURL=main.js.map
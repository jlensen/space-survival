/// <reference path='gameItem.ts' />

class Character extends GameItem {
    
    //holds a reference to the bullet pool in the game class
    private _bulletPool: Bullet[];
    //holds the delay between shooting two bullets
    private _bulletTime: number;
    //holds the time left until the player isn't invincible anymore
    private _invincibleTime: number;
    //boolean that holds the state of the player. Is true if the player is dead, false if alive
    private _dead: boolean;
    //holds the players orientation. is true if facing up, false if facing down
    private _up: boolean;
    
    constructor(name:string, x: number, y: number, bulletPool: Bullet[]) {
        super(x, y, name); 
        this._bulletPool = bulletPool;
        this._bulletTime = 0;
        this._invincibleTime = 0;
        this._dead = false;
        this._up = false;
    }

    //moves the character right
    public right = () => {
        this.move(8,0);
        if (this._xPos > document.body.clientWidth - this._element.clientWidth) {
            this._xPos = document.body.clientWidth - this._element.clientWidth;
        }
    }

    //moves the character left
    public left = () => {
        this.move(-8,0);
        if (this._xPos < 0) {
            this._xPos = 0;
        }
    }

    //moves the character up
    public up = () => {
        this.move(0,-8);
        if (this._yPos < 0) {
            this._yPos = 0;
        }
        if (!this._up) {
            this._element.style.transform = "scaleY(-1)";
            this._up = true;
        }
    }

    //moves the character down
    public down = () => {
        this.move(0,8);
        if (this._yPos > document.body.clientHeight - this._element.clientHeight) {
            this._yPos = document.body.clientHeight - this._element.clientHeight;
        }
        if (this._up) {
            this._element.style.transform = "scaleY(1)";
            this._up = false;
        }
    }

    //shoots a bullet in the direction the player is facing
    public shoot = () => {
        if (!this._dead) {
            if (this._bulletTime === 0) {
                let bullet;
                if (this._up) {
                    bullet = new Bullet(true);
                    bullet.setPosition(this._xPos + this._element.clientWidth / 2 - bullet.width / 2, this._yPos - 10);
                }
                else {
                    bullet = new Bullet(false);
                    bullet.setPosition(this._xPos + this._element.clientWidth / 2 - bullet.width / 2, this._yPos + this._element.clientHeight + 5);
                }
                bullet.element.style.display = "block";
                this._bulletPool.push(bullet);
                this._bulletTime = 40;
                console.log(this._name + " shoots");
            }
        }
    }

    //makes the character flicker to signal the character is invincible
    public invincible() {
        if (this._element.style.display === "none") {
            this._element.style.display = "block";
        } else {
            this._element.style.display = "none";
        }
    }

    //gets the character element
    get element() {
        return this._element;
    }

    //gets the bulletTime
    get bulletTime() {
        return this._bulletTime;
    }

    //sets the bulletTime
    set bulletTime(amount: number) {
        this._bulletTime = amount;
    }

    //gets the invincibleTime
    get invincibleTime() {
        return this._invincibleTime;
    }

    //sets the invincibleTime
    set invincibleTime(amount: number) {
        this._invincibleTime = amount;
    }

    //sets the bulletpool of the player
    set bulletPool(bulletPool: Bullet[]) {
        this._bulletPool = bulletPool;
    }

    //sets the status of the player to dead
    set dead(status: boolean) {
        this._dead = status;
    }
}
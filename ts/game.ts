class Game {
	// The element in which the game is displayed
	private _element: HTMLElement = document.getElementById('container');
	//player 1 character
	private _character1: Character;
	//player 2 character
	private _character2: Character;
	//handles key input for players
	private keyHandler: KeyHandler;
	//holds all the bullets currently in the game
	private bulletPool: Bullet[] = [];
	//holds all the obstacles currently in the game
	private obstaclePool: Obstacle[] = [];
	//holds the position of the wallpaper
	private _wallpap: number;
	//holds the hud
	private _hud: HUD;
	//which player won the game
	private _winner: string;
	//determines if the update function is paused or not
	private _pause: boolean;
	//audio element for the game
	private _audio: HTMLAudioElement;

	constructor() {
		this._wallpap = 0;
		this._winner = "";
		this._pause = false;

		this._hud = new HUD();

		
		//update add the end
		this.init();
		this.update();
	}

	//initializes everything that's neede for the game to run
	public init(): void {
		this._element.style.width = document.body.clientWidth + "px";
		this._element.style.height = document.body.clientHeight + "px";

		//initializes controls
		this.keyHandler = new KeyHandler();
		this._character1 = new Character('character1', 100, 400, this.bulletPool);
		this._character2 = new Character('character2', 1000, 400, this.bulletPool);
		this._character1.draw(this._element);
		this._character2.draw(this._element);
		this.keyHandler.addCallback(39, this._character1.right);
		this.keyHandler.addCallback(37, this._character1.left);
		this.keyHandler.addCallback(38, this._character1.up);
		this.keyHandler.addCallback(40, this._character1.down);
		this.keyHandler.addCallback(77, this._character1.shoot);
		this.keyHandler.addCallback(65, this._character2.left);
		this.keyHandler.addCallback(68, this._character2.right);
		this.keyHandler.addCallback(87, this._character2.up);
		this.keyHandler.addCallback(83, this._character2.down);
		this.keyHandler.addCallback(67, this._character2.shoot);

		this._hud.init(this._element);

		this._audio = new Audio('assets/sound/150413_Magical_Night---free_download.mp3');
		this._audio.play();
	}

	//handles everything that is updated every frame. This is essentially the game loop
	public update = (): void =>  {
		if (!this._pause) {
			requestAnimationFrame(this.update);
			this.background();
			this.keyHandler.inputHandler();

			this.bulletHandler();
			this.obstacleCreator();
			this.obstacleHandler();
			this.collisionHandler();
		}
	}

	//handles all the moving bullets
	private bulletHandler(): void {
		for (let i = 0; i < this.bulletPool.length; i++) {
			if (this.bulletPool[i] === null) {
				continue;
			}


			if (this.bulletPool[i].element.parentElement === null) {
				this._element.appendChild(this.bulletPool[i].element);
			}
			else {
				this.bulletPool[i].draw();
				if (this.bulletPool[i].yPos < 0 || this.bulletPool[i].yPos > document.body.clientHeight) {
					this._element.removeChild(this.bulletPool[i].element);
					this.bulletPool[i] = null;
				}
			}
		}
		this.bulletPool = this.bulletPool.filter(x => x !== null);
		this._character1.bulletPool = this.bulletPool;
		this._character2.bulletPool = this.bulletPool;

		if (this._character1.bulletTime != 0) {
			this._character1.bulletTime = this._character1.bulletTime - 1;
		}
		if (this._character2.bulletTime != 0) {
			this._character2.bulletTime = this._character2.bulletTime - 1;
		}
		if (this._character1.invincibleTime != 0) {
			this._character1.invincibleTime = this._character1.invincibleTime - 1;
			this._character1.invincible();
		}
		if (this._character2.invincibleTime != 0) {
			this._character2.invincibleTime = this._character2.invincibleTime - 1;
			this._character2.invincible();
		}
	}

	//handles all collisions in the game
	private collisionHandler(): void {

		//collisions between players and bullets
		for (let i = 0; i < this.bulletPool.length; i++) {
			if (((this.bulletPool[i].yPos < this._character1.yPos + this._character1.element.clientHeight) && (this.bulletPool[i].yPos > this._character1.yPos)) && ((this.bulletPool[i].xPos < this._character1.xPos + this._character1.element.clientWidth) && (this.bulletPool[i].xPos > this._character1.xPos)) && this._character1.invincibleTime === 0) {
				console.log("player1 hit by bullet");
				this._hud.removeHeart(true);
				this._character1.invincibleTime = 100;
				if (this._hud.getLives(true) === 0) {
					this._element.removeChild(this._character1.element);
					this._character1.dead = true;
					this._winner = "Player 2";
					this.win();
				}
			}
			if (((this.bulletPool[i].yPos < this._character2.yPos + this._character2.element.clientHeight) && (this.bulletPool[i].yPos > this._character2.yPos)) && ((this.bulletPool[i].xPos < this._character2.xPos + this._character2.element.clientWidth) && (this.bulletPool[i].xPos > this._character2.xPos)) && this._character2.invincibleTime === 0) {
				console.log("player2 hit by bullet");
				this._hud.removeHeart(false);
				this._character2.invincibleTime = 100;
				if (this._hud.getLives(false) === 0) {
					this._element.removeChild(this._character1.element);
					this._character1.dead = true;
					this._winner = "Player 1";
					this.win();
				}
			}
		}

		//collisions between players and obstacles
		for (let i = 0; i < this.obstaclePool.length; i++) {
			if ((((this._character1.yPos >= this.obstaclePool[i].yPos && this._character1.yPos <= this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight) || (this._character1.yPos + this._character1.element.clientHeight >= this.obstaclePool[i].yPos && this._character1.yPos + this._character1.element.clientHeight <= this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight)) || ((this.obstaclePool[i].yPos >= this._character1.yPos && this.obstaclePool[i].yPos <= this._character1.yPos + this._character1.element.clientHeight) || (this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight >= this._character1.yPos && this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight <= this._character1.yPos + this._character1.element.clientHeight))) && (this.obstaclePool[i].xPos >= this._character1.xPos && this.obstaclePool[i].xPos <= this._character1.xPos + this._character1.element.clientWidth) && this._character1.invincibleTime === 0) {
				console.log("player1 hit by obstacle");
				this._hud.removeHeart(true);
				this._character1.invincibleTime = 100;
				if (this._hud.getLives(true) === 0) {
					this._element.removeChild(this._character1.element);
					this._character1.dead = true;
					this._winner = "Player 2";
					this.win();
				}
			}
			if ((((this._character2.yPos >= this.obstaclePool[i].yPos && this._character2.yPos <= this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight) || (this._character2.yPos + this._character2.element.clientHeight >= this.obstaclePool[i].yPos && this._character2.yPos + this._character2.element.clientHeight <= this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight)) || ((this.obstaclePool[i].yPos >= this._character2.yPos && this.obstaclePool[i].yPos <= this._character2.yPos + this._character2.element.clientHeight) || (this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight >= this._character2.yPos && this.obstaclePool[i].yPos + this.obstaclePool[i].element.clientHeight <= this._character2.yPos + this._character2.element.clientHeight))) && (this.obstaclePool[i].xPos >= this._character2.xPos && this.obstaclePool[i].xPos <= this._character2.xPos + this._character2.element.clientWidth) && this._character2.invincibleTime === 0) {
				console.log("player2 hit by obstacle");
				this._hud.removeHeart(false);
				this._character2.invincibleTime = 100;
				if (this._hud.getLives(false) === 0) {
					this._element.removeChild(this._character1.element);
					this._character1.dead = true;
					this._winner = "Player 1";
					this.win();
				}
			}
		}
		
	}

	//creates the moving obstacles for the game
	private obstacleCreator(): void {
		//generates a number to determine the chance of spawning an obstacle
		let random = Math.random() * 600;
		if (random > 595) {
			//width and height of the obstacle are randomly generated, although it makes sure they aren't below 20
			let sides = Math.random() * 200;
			while (sides < 20) {
				sides = Math.random() * 200;
			}

			let obstacle = new Obstacle(sides, sides);

			//generate random position on the right of the screen
			let pos = Math.random() * document.body.clientHeight;
			obstacle.setPosition(document.body.clientWidth, pos);
			this._element.appendChild(obstacle.element);
			this.obstaclePool.push(obstacle);
		}
	}

	//handles the moving obstacles
	private obstacleHandler(): void {
		for (let i = 0; i < this.obstaclePool.length; i++) {
			this.obstaclePool[i].draw();
			if (this.obstaclePool[i].xPos < 0) {
				this._element.removeChild(this.obstaclePool[i].element);
				this.obstaclePool[i] = null;
			}
		}
		this.obstaclePool = this.obstaclePool.filter(x => x !== null);
	}

	//handles the moving background
	private background(): void {
		this._wallpap -= 1;
		let background = document.getElementById("container");
		background.style.backgroundPosition = this._wallpap + 'px 0';

	}

	//stops the game and displays the win screen
	private win(): void {
		this._pause = true;
		this._element.innerHTML = "";
		let wintext = document.createElement("div");
		let text = document.createElement("p");
		text.innerHTML = "The winner is: " + this._winner + "<br>Herladen om nog een keer te spelen.";
		wintext.appendChild(text);
		wintext.id = "winsign";
		this._element.appendChild(wintext);

		this._audio.pause();
	}


}
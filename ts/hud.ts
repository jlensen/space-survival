class HUD {

    //div that holds the lives of player1
    private _player1life: HTMLElement;
    //div that holds the lives of player1
    private _player2life: HTMLElement;

    //initializes the hud
    public init(location: HTMLElement) {
        this._player1life = document.createElement("div");
        this._player1life.classList.add("life");
        this._player2life = document.createElement("div");
        this._player2life.classList.add("life");

        let p1 = document.createElement("p");
        let p2 = document.createElement("p");
        p1.innerText = "Levens";
        p2.innerText = "Levens";

        this._player1life.appendChild(p1);
        this._player2life.appendChild(p2);
        this._player1life.style.left = 0 + "px";
        this._player1life.style.top = 0 + "px";

        let lifebox = [];
        for (let i = 0; i < 3; i++) {
            let box = document.createElement("div");
            box.classList.add("lifebox")
            lifebox.push(box);
        }
        
        let lifebox2 = [];
        for (let i = 0; i < 3; i++) {
            let box = document.createElement("div");
            box.classList.add("lifebox")
            lifebox2.push(box);
        }

        for (let i = 0; i < lifebox.length; i++) {
            this._player1life.appendChild(lifebox[i]);
        }

        for (let i = 0; i < lifebox2.length; i++) {
            this._player2life.appendChild(lifebox2[i]);
        }

        location.appendChild(this._player2life);
        this._player2life.style.left = document.body.clientWidth - this._player2life.clientWidth + "px";

        location.appendChild(this._player1life);
    }

    //removes a heart fromt a player. parameter playerone determines the player. player1 if true, player2 if false
    public removeHeart(playerone: boolean) {
        if (playerone) {
            let lifeboxes = this._player1life.getElementsByClassName("lifebox");
            this._player1life.removeChild(lifeboxes[0]);
        }
        else {
            let lifeboxes = this._player2life.getElementsByClassName("lifebox");
            this._player2life.removeChild(lifeboxes[0]);
        }
    }

    //returns the lives of a player. parameter playerone determines the player. player1 if true, player2 if false
    public getLives(playerone: boolean) {
        if (playerone) {
            return this._player1life.getElementsByClassName("lifebox").length;
        }
        else {
            return this._player2life.getElementsByClassName("lifebox").length;
        }
    }
}
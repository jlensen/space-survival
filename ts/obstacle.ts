class Obstacle extends GameItem {
    
    //width of the obstacle
    private _width: number;
    //height of the obstacle
    private _height: number;

    constructor(width: number, height: number) {
        super(0,0,"obstacle")

        //creates the dom element for the obstacle
        this._height = height;
        this._width = width;
        this._element = document.createElement("div");
        this._element.style.width = this._width + "px";
        this._element.style.height = this._height + "px";
        let image = document.createElement("img");
        image.src = "./assets/images/asteroid.png";
        image.style.height = "100%";
        this._element.appendChild(image);
        this._element.style.position = "absolute";
    }

    public setPosition(x: number, y: number) {
        this._xPos = x;
        this._yPos = y;
    }

    public draw() {
        this.move(-10,0);
    }


}
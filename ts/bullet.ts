/// <reference path='gameItem.ts' />

class Bullet extends GameItem {

    private _width: number;
    private _height: number;

    //true if bullet goes up, false is bullet goes down
    private _up: boolean;

    constructor(up: boolean) {
        super(-20,-20,"bullet");
        this._width = 10;
        this._height = 20;
        this._element = document.createElement("div");
        this._element.style.width = this._width + "px";
        this._element.style.height = this._height + "px";
        this._element.style.display = "none";
        this._element.style.position = "absolute";
        this._element.style.backgroundImage = "url('../assets/images/laser.png')";
        this._up = up;
    }

    public setPosition(x: number, y: number) {
        this._xPos = x;
        this._yPos = y;
    }

    public draw() {
        if (this._up) {
            this.move(0,-10);
        }
        else {
            this.move(0,10);
        }
    }

    get width() {
        return this._width;
    }

}
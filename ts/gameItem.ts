class GameItem {

    protected _element:HTMLElement;
    protected _yPos: number;
    protected _xPos: number;
    protected _name: string;

    constructor(x: number, y: number, name: string) {
        this._xPos = x;
        this._yPos = y;
        this._name = name;
    }

    get element() {
        return this._element;
    }

    get xPos(): number {
        return this._xPos;
    }

    get yPos(): number {
        return this._yPos;
    }

    public draw(location: HTMLElement) {
        this._element = document.createElement("div");
        let image = document.createElement("img");
        image.setAttribute("src", "../assets/images/" + this._name + ".png");
        this._element.className = "character";
        image.style.width = "100%";
        this._element.style.position = "absolute";
        this._element.style.top = this._yPos + "px";
        this._element.style.left = this._xPos + "px";
        this._element.appendChild(image);
        location.appendChild(this._element);
    }

    public move(amountx: number, amounty: number) {
        this._xPos += amountx;
        this._yPos += amounty;
        this._element.style.left = this._xPos + "px";
        this._element.style.top = this._yPos + "px";
    }
}
//this is my constant where game and events meet
let app: any;

(function () {
    /**
     * Run after dom is ready
     */
    let init = function () {
        document.getElementById("container").innerHTML = "";
        app = new Game();
    };

    document.getElementById("startbutton").addEventListener('click', init);
})();
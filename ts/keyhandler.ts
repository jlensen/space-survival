class KeyHandler {
    private _callBacks: {[keycode: number]: () => void; } = {};
    private _keys: { [keycode: number]: boolean;} = {};

    constructor() {
        window.addEventListener("keydown", this.keyDownHandler);
        window.addEventListener("keyup", this.keyUpHandler);
    }

    public keyDownHandler = (event: KeyboardEvent) => {
        if (this._callBacks[event.keyCode] != null) {
            this._keys[event.keyCode] = true;
        }
    }

    public keyUpHandler = (event: KeyboardEvent): void => {
        if (this._callBacks[event.keyCode] != null) {
            this._keys[event.keyCode] = false;
        }
     }

    public addCallback(keycode: number, func: () => void) {
        this._callBacks[keycode] = func;
    }

    public inputHandler() {
        for (let key in this._keys) {
            if (this._keys[key] === true) {
                this._callBacks[key]();
            }
        }
    }
}